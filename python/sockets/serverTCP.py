import socket


class TcpServer:

    def __init__(self, ip="192.168.31.60", port=5005):
        self.ip = ip
        self.port = port
        self.message = chr(2)+chr(83)+chr(32)+chr(48)+chr(48)+chr(50)+chr(53)+chr(44)+chr(50)+chr(53)+chr(3)

    # Create a TCP/IP socket
    def run(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Bind the socket to the port
        server_address = (self.ip, self.port)
        print('starting up on {} port {}'.format(*server_address))
        sock.bind(server_address)

        # Listen for incoming connections
        sock.listen(12)

        while True:
            # Wait for a connection
            print('[+] Waiting for a connection...')
            connection, client_address = sock.accept()
            try:
                print('[+] Got a connection from: ', client_address)

                # Receive the data in small chunks and retransmit it
                message = self.message
                while True:
                    connection.sendall(message.encode())
            except ConnectionResetError:
                print("[+] Connection finished by client.")
            finally:
                # Clean up the connection
                connection.close()
